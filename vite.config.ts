import { fileURLToPath, URL } from "node:url";
import vue from "@vitejs/plugin-vue";
import { defineConfig } from "vite";
import AutoImports from "unplugin-auto-import/vite";
import Pages from "vite-plugin-pages";
import Layouts from "vite-plugin-vue-layouts";
import { DirResolverHelper } from "vite-auto-import-resolvers";
import TypeAutoImports from "vite-plugin-type-auto-imports";
// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 3000,
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        changeOrigin: true,
      },
    }
  },
  plugins: [
    Pages({
      dirs: "src/views",
    }),
    Layouts(),
    vue(),
    DirResolverHelper(),
    AutoImports({
      include: [
        /\.[tj]sx?$/, // .ts, .tsx, .js, .jsx
        /\.vue$/,
        /\.vue\?vue/, // .vue
      ],
      imports: ["vue", "vue-router", "pinia", "vue-i18n"],
      vueTemplate: true,
      dirs: [
        "src/stores/*",
        "src/composables/*",
        "src/shared/models/*",
        "src/shared/services/*",
      ],
      dts: true,
    }),
    TypeAutoImports({
      extensions: ["ts", ".jsx"],
      nodeModules: ["vue"],
      excludeDirs: [],
    }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});
