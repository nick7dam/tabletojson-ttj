interface AppStoreInterface {
  serverPath: string;
}
const useAppStore = defineStore({
  id: "AppStore",
  state: () =>
      ({
        serverPath: "http://localhost:8080",
      } as AppStoreInterface),
  getters: {
    getServerPath: (state) => state.serverPath,
  },
  actions: {
    setServerPath(serverPath: string) {
      this.serverPath = serverPath;
    }
  },
});
export default useAppStore;
