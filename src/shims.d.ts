declare interface Window {
  // extend the window
}
declare module "*.vue" {
  const component: ReturnType<any>;
  export default component;
}
declare module "virtual:generated-layouts";
declare module "virtual:generated-pages";
