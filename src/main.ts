import App from "./App.vue";
import router from "./router";
import en from "./translations/en/en";
import es from "./translations/es/es";
import "./assets/scss/main.scss";
import { createI18n } from "vue-i18n";
const app = createApp(App);
const messages = {
  en: en,
  es: es,
};
const i18n = createI18n({
  legacy: false,
  locale: "en",
  fallbackLocale: "en",
  messages,
});
app.use(i18n);
app.use(createPinia());
app.use(router);
app.mount("#app");
