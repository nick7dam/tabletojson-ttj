import { zonedTimeToUtc, utcToZonedTime } from "date-fns-tz";
import {
  sub,
  add,
  format,
  parseISO,
  differenceInDays,
  differenceInMonths,
  differenceInHours,
  differenceInMilliseconds,
  differenceInMinutes,
  differenceInSeconds,
  differenceInWeeks,
  differenceInYears,
  differenceInBusinessDays,
} from "date-fns";

export abstract class DateTimeService {
  abstract parseISO: any;
  abstract format: any;
  abstract add: any;
  abstract substract: any;
  abstract diff: any;
  abstract utcToLocal: any;
  abstract localToUtc: any;
}

export const DTService: DateTimeService = class {
  static parseISO(date: string) {
    return parseISO(date);
  }

  static format(date: any, format_value: string) {
    return format(date, format_value);
  }

  static add(date: Date, value: string, amount: number) {
    return add(date, { [value]: amount });
  }

  static substract(date: Date, value: string, amount: number) {
    return sub(date, { [value]: amount });
  }

  static diff(startDate: Date, endDate: Date, type: string) {
    switch (type) {
      case "years":
        return differenceInYears(startDate, endDate);
      case "months":
        return differenceInMonths(startDate, endDate);
      case "weeks":
        return differenceInWeeks(startDate, endDate);
      case "days":
        return differenceInDays(startDate, endDate);
      case "businessDays":
        return differenceInBusinessDays(startDate, endDate);
      case "hours":
        return differenceInHours(startDate, endDate);
      case "minutes":
        return differenceInMinutes(startDate, endDate);
      case "seconds":
        return differenceInSeconds(startDate, endDate);
      case "milliseconds":
        return differenceInMilliseconds(startDate, endDate);
    }
  }
  static utcToLocal(date: string) {
    const store = useAppStore();
    console.log(store.selectedLang)
    return utcToZonedTime(date, store.getTimezone);
  }
  static localToUtc(date: string) {
    const store = useAppStore();
    return zonedTimeToUtc(date, store.getTimezone);
  }
};
