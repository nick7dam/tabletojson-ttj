export abstract class Helpers {
  abstract firstLetterToUpperCase: any;
}

export const useHelpers: Helpers = class {
  static firstLetterToUpperCase(str: string) {
    return str.charAt(0).toUpperCase() + str.substring(1);
  }
};
